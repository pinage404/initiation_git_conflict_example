def fizzBuzz(number: int) -> str:
    if number % 3 == 0: return "Fizz"
    return f"{number}"
